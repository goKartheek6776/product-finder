import React, { Component } from 'react';
import './style.css';

import ProductResults from './../ProductResults';
import Checkbox from './../Checkbox';

class ProductSearch extends Component {
    constructor(props) {
        super(props);
        this.state = {
          searchQuery : '',
          products: [],
          inStockOnly: false,
        }
      }

      componentDidMount() {
          fetch('https://api.myjson.com/bins/109m7i')
            .then(response => response.json())
            .then(data => {
                this.setState({
                    products: data,
                })
            })
      }
    
      filterProducts = (event) => {
        this.setState({
          searchQuery: event.target.value
        })
      }

      showInStockOnly = (event) => {
        this.setState({
            inStockOnly: event.target.checked 
        })
      }

    render() {
        let { searchQuery, products, inStockOnly } = this.state;
        return (
            <div className="product-search-wrapper">
                <div className="title">Product Search</div>
                <div>
                    <div className="product-search">
                        <input
                            type="text"
                            className="product-input"
                            placeholder="Search products.." 
                            value={searchQuery} 
                            onChange={this.filterProducts}
                        />
                    </div>
                    <div className="stock-check">
                        <Checkbox
                            checked={this.state.inStockOnly}
                            onChange={this.showInStockOnly}
                        />
                        <span>Only show products in stock </span>
                    </div>
                </div>
                {
                    products && products.length > 0 &&
                    <ProductResults
                        products={products} 
                        inStockOnly={inStockOnly} 
                        searchQuery={searchQuery}
                    />
                }
            </div>
        );
    }
}

export default ProductSearch;