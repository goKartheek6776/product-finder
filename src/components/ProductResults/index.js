import React from 'react';
import './style.css';

const ProductResults = (props) => {
    let categories = [];
    let { products } = props;
    
    if(props.inStockOnly) {
        products = products.filter(product => product.stocked);
    }

    if(props.searchQuery.length > 0) {
        products = products.filter(product => product.name.includes(props.searchQuery));
    }
    // get unique categories
    products.forEach(product => categories.push(product.category));
    categories = [...new Set(categories)];

    let finalList = [], finalProducts = [];
    categories.forEach((category) => {
        finalProducts = products.filter(product => product.category === category);
        finalList.push({
            'category' : category,
            'products' : finalProducts
        })  
    });

    return(
        <div className="product-results-wrapper"> 
            <div className="header">
                <div>Name</div>
                <div>Price</div>
            </div>

            {
                finalList.map((set,index) => {
                    return(
                        <div className="results-section" key={`row-${index}`}>
                            <div className="category-name">{set.category}</div>
                            <div className="products">
                                {
                                    set.products.map((product, i) => {
                                        return(
                                            <div className="product-row" key={`product-row-${i}`}>
                                                <div className={product.stocked ? '' : 'unavailable'}>{product.name}</div>
                                                <div>{product.price}</div>
                                            </div>
                                        );
                                    })
                                }
                            </div>
                        </div>
                    )
                })
            }
        </div>
    );
}

export default ProductResults;