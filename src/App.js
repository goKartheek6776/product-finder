import React from 'react';
import './App.css';
import ProductSearch from './components/ProductSearch';

const App = () => {
  return (
    <div className="container">
        <ProductSearch />
    </div>
  );
} 

export default App;
